import sys
from binaryninja import *

def get_func_params(bp_addr):
    xref = xref_db.get(bp_addr)
    il = xref.function.get_low_level_il_at(xref.address).mlil
    if il.operation == MediumLevelILOperation.MLIL_CALL:
        offset = il.params[0].value.value

    return offset

def label_data_offsets(current_data_offset, decrypted_str):
    sym = Symbol(SymbolType.DataSymbol, current_data_offset, "{0}".format(decrypted_str.decode("utf8")))
    bv.define_user_symbol(sym)

    # Patch decrypted bytes as well
    bw.seek(current_data_offset)
    bw.write(decrypted_str)

    bv.save_auto_snapshot()

def main():
    global bv
    global bw
    global xref_db

    # Add plugins directory so we can import the debugger
    # TODO: Need to make this generic so it adds the correct path based on os
    plugin_path = "C:\\Users\\<username>\\AppData\\Roaming\\Binary Ninja\\plugins"
    sys.path.append(plugin_path)

    try:
        from debugger import DebugAdapter, dbgeng
    except:
        sys.exit(-1)
    
    # Working Binja database to modify
    bndb = "main_bin-BN.bndb"
    bv = BinaryViewType.get_view_of_file("main_bin-BN.bndb")
    bv.update_analysis()
    
    bv.create_database(bndb)

    # Helpers making it easier to modifying the database
    bw = BinaryWriter(bv)

    # Load sample in debugger
    pe = "C:\\Users\\<username>\\Desktop\\main_bin.exe"
    adapter = dbgeng.DebugAdapterDbgeng()
    adapter.exec(pe, args=None)

    # xrefs to set the breakpoints on
    xref_db = {}
    mw_api_name_decrypt_addr = 0x401300
    xrefs = bv.get_code_refs(mw_api_name_decrypt_addr)
    for xref in xrefs:
        adapter.breakpoint_set(xref.address)
        xref_db[xref.address] = xref
    
    # Execute until we hit our breakpoints
    while adapter.breakpoint_list() != None:
        reason, data = adapter.go()   
        if reason == DebugAdapter.STOP_REASON.BREAKPOINT:
            current_bp = adapter.get_last_breakpoint_address()
            current_data_offset = get_func_params(current_bp)

            # Determine the length of encrypted data stored at the current data variable
            next_offset = bv.get_next_data_var_after(current_data_offset)
            length = next_offset.address - current_data_offset

            adapter.step_over()

            # Get decrypted strings and write them to the data base
            decrypted_str = adapter.mem_read(current_data_offset, length)
            print("{0}:{1}".format(hex(current_data_offset), decrypted_str.decode("utf8")))

            label_data_offsets(current_data_offset, decrypted_str)
            
            adapter.breakpoint_clear(current_bp)

if __name__ == "__main__":
    try:
        # The OG process is killed once names resolved so needed a cheap trick
        # to catch the CTRL+C interrupt to finish. The database has already been
        # saved at this point
        main()
    except KeyboardInterrupt:
        sys.exit(-1)    