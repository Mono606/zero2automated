import binascii
from binaryninja import *

# Ref: https://github.com/securitykitten/shellcode_hashes

exports_db = {}

def setup_db(exports):
    global exports_db

    with open(exports, mode="r") as f:
        for line in f:
            try:
                line_split = line.strip("\n").split(",")
                dll_import = line_split[0]
                crc32 = line_split[1]
                exports_db[crc32] = dll_import

            except IndexError:
                continue

def label_data_offsets(bv, func):
    for il in func[0].mlil.instructions:
        try:
            if il.operation == MediumLevelILOperation.MLIL_CALL:
                # This is where the data offset points to after name resolution
                src = il.operands[0][0].name 
                api_const = il.params[1].value.value
                if api_const != None:
                    # Used to rename the data offset to resolved api name
                    resolved = api_name_resolve(api_const) 
                    print("{0}:{1}".format(hex(api_const), resolved))
            
            if il.operation == MediumLevelILOperation.MLIL_STORE:
                if il.operands[1].src.name == src:
                    data_offset = il.operands[0].value.value
                    # Rename symbol with resolved api name
                    sym = Symbol(SymbolType.DataSymbol, data_offset, resolved)
                    bv.define_user_symbol(sym)

        except IndexError:
            continue
            
def api_name_resolve(checksum):
    return exports_db.get(hex(checksum))

def main():
    setup_db("exports_crc32.txt")

    bndb_path = "cruloader-BN.bndb"
    bv = BinaryViewType.get_view_of_file(bndb_path)
    bv.update_analysis()

    bv.create_database(bndb_path)
    
    # Find address to api name resolve function by searching for the constant
    # 0x78 which represents the offset into the Export directory of the loaded 
    # module's PE header
    export_offset_addr = bv.find_next_constant(bv.offset, 0x78)
    
    # Start address of the api name resolve function
    mw_api_name_resolve_func = bv.get_functions_containing(export_offset_addr)
    mw_api_name_resolve_addr = mw_api_name_resolve_func[0][0].start 
    
    xrefs = bv.get_code_refs(mw_api_name_resolve_addr)
    
    # Use Medium IL to grab the crc32 constants off the stack. There are two functions 
    # where some of the results are saved to data offsets which are called by other functions.
    # For those cases we are going to write the resolved api name to data offset otherwise 
    # save it as a comment at the address of resolution.
    for xref in xrefs:
        func = bv.get_functions_containing(xref.address)
        if func[0].start == 0x401d50:
            label_data_offsets(bv, func)
        elif func[0].start == 0x401dc0:
            label_data_offsets(bv, func)
        else:
            il = xref.function.get_low_level_il_at(xref.address).mlil
            if il.operation == MediumLevelILOperation.MLIL_CALL:
                api_const = il.params[1].value.value
                resolved = api_name_resolve(api_const)
                bv.set_comment_at(xref.address, resolved)
                print("{0}:{1}".format(hex(api_const), resolved))
    
    # Don't forget to grab the crc32 from the crc32_lookup_function. It resolves to "svchost.exe"
    scvhost_crc32_addr = bv.find_next_constant(0x400000, 0xb925c42d)
    if scvhost_crc32_addr != None and binascii.crc32(b"svchost.exe") == 0xb925c42d:
        bv.set_comment_at(scvhost_crc32_addr, "svchost.exe")
    
    bv.save_auto_snapshot()

if __name__ == "__main__":
    main()