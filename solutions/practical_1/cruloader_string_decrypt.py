import sys
import struct
from binaryninja import *

def label_data_offset(data_offset, decrypted_str):
    sym = Symbol(SymbolType.DataSymbol, data_offset, "{0}".format(decrypted_str.decode("utf8")))
    bv.define_user_symbol(sym)

    bw.seek(data_offset)
    bw.write(decrypted_str)

    bv.save_auto_snapshot()

def decrypt_download_url(url_data_offset, lstrlen_addr):
    # Get encrypted string length from `lstrlenA()`
    adapter.breakpoint_set(lstrlen_addr)
    adapter.go()
    adapter.step_over()
    length = adapter.reg_read('eax')
    adapter.breakpoint_clear(lstrlen_addr)

    # Get decrypted string off stack after decoding loop
    adapter.breakpoint_set(0x401e73)
    adapter.go()
    esp = adapter.reg_read('esp')
    decrypted_url = adapter.mem_read(esp, length)
    print("{0}:{1}".format(hex(url_data_offset), decrypted_url))
    label_data_offset(url_data_offset, decrypted_url)
    adapter.breakpoint_clear(0x401e73)

def decrypt_save_to_disk_strings():
    eax = adapter.reg_read('eax')
    url = adapter.mem_read(eax, 40)
    print("{0}:{1}".format(hex(eax), url))
    data_offset = 0x413ca4 # label this in Binja
                
    # Get ptr to encrypted string off stack 
    lstrlen_addr = 0x4013eb
    adapter.breakpoint_set(0x4013E6) #
    adapter.go()
    esp = adapter.reg_read('esp')
    adapter.step_over()
    adapter.step_over()
    adapter.breakpoint_clear(0x4013E6)
    
    # Get encrypted string length from lstrlenA()
    length = adapter.reg_read('eax')

    # Get decrypted filename off stack after decoding loop                
    adapter.breakpoint_set(0x401406)                
    adapter.go()
    encrypted_filename_ptr = adapter.mem_read(esp, 4)
    convert_address_bigendian = struct.unpack('<I', encrypted_filename_ptr)
    for addr in convert_address_bigendian:
        decrypted_filename_addr = addr
    decrypted_filename = adapter.mem_read(decrypted_filename_addr, length)
    decrypted_filename_mod = decrypted_filename[1:11] # getting rid of leading '\\'
    print("{0}:{1}".format(hex(data_offset) ,decrypted_filename_mod))
    bv.set_comment_at(0x4013E6, decrypted_filename_mod)
    bv.save_auto_snapshot()
    label_data_offset(data_offset, decrypted_filename_mod)
    lstrlen_addr = 0x401541
    adapter.breakpoint_set(lstrlen_addr)
    adapter.breakpoint_clear(0x401406)
    
    # Get second encrypted string ptr
    adapter.go()
    esp = adapter.reg_read('esp')
    
    # Step over next two addresses to encrypted string length
    adapter.step_over()
    adapter.step_over()
    length = adapter.reg_read('eax')
    
    # Execute to address just past the second decoding loop to get decrypted string
    adapter.breakpoint_set(0x401563)
    adapter.go()
    encrypted_str_ptr = adapter.mem_read(esp, 4)
    convert_address_bigendian = struct.unpack('<I', encrypted_str_ptr)
    for addr in convert_address_bigendian:
        decrypted_str_addr = addr
    decrypted_str = adapter.mem_read(decrypted_str_addr, length)
    print("{0}:{1}".format(hex(lstrlen_addr),decrypted_str))
    bv.set_comment_at(lstrlen_addr, decrypted_str)
    adapter.breakpoint_clear(0x401563)
    bv.save_auto_snapshot()

def decrypt_process_name():
    adapter.step_into()
    adapter.breakpoint_set(0x401CF3)
    adapter.go()
    
    # Get encrypted process name length
    adapter.step_over()
    esp = adapter.reg_read('esp') 
    adapter.step_over() # step over lstrlenA
    length = adapter.reg_read('eax')

    # Execute to instruction past decryption loop
    adapter.breakpoint_set(0x401D13)
    adapter.breakpoint_clear(0x401CF3)
    adapter.go()

    # Get decrypted process name
    data_offset = 0x413c5c
    encrypted_proc_name_ptr = adapter.mem_read(esp, 4)
    convert_address_bigendian = struct.unpack('<I', encrypted_proc_name_ptr)
    for addr in convert_address_bigendian:
        decrypted_proc_name_addr = addr
    decrypted_proc_name = adapter.mem_read(decrypted_proc_name_addr, length)
    print("{0}:{1}".format(hex(data_offset), decrypted_proc_name))
    decrypted_proc_name_mod = decrypted_proc_name[20:31]
    label_data_offset(data_offset, decrypted_proc_name_mod)
    bv.save_auto_snapshot()

def main():
    global bv
    global bw
    global adapter
    
    plugin_path = "C:\\Users\\<username>\\AppData\\Roaming\\Binary Ninja\\plugins"
    sys.path.append(plugin_path)

    try:
        from debugger import DebugAdapter, dbgeng
    except:
        sys.exit(-1)

    # Working Binja database to modify
    bndb = "cruloader-BN.bndb"
    bv = BinaryViewType.get_view_of_file(bndb)
    bv.update_analysis()

    bv.create_database(bndb)

    # Helpers making it easier to modifying the database
    bw = BinaryWriter(bv)

    # Load sample in debugger
    pe = "C:\\Users\\<username>\\Desktop\cruloader.exe"
    adapter = dbgeng.DebugAdapterDbgeng()
    adapter.exec(pe, args=None)

    # Initial breakpoints
    # 0x401f14 -> 'svchost.exe' hash compare (cmp eax, 0xb925c42d)
    # 0x401dc0 -> mw_command_control()
    # 0x4013a0 -> mw_save_to_disk()
    # 0x401607 -> mw_start_process()
    breakpoints = [0x401f14, 0x401dc0, 0x4013a0, 0x401607]
    for bp in breakpoints:
        adapter.breakpoint_set(bp)

    # Execute until we hit the breakpoint
    while adapter.breakpoint_list() != None:
        reason, data = adapter.go()
        if reason == DebugAdapter.STOP_REASON.BREAKPOINT:
            current_bp = adapter.get_last_breakpoint_address()
            
            # Pass the 'svchost.exe' crc32 check
            if current_bp == 0x401f14:
                adapter.reg_write('eax', 0xb925c42d)
                adapter.breakpoint_clear(current_bp)
                adapter.step_over()
            
            # Decrypting url in mw_command_control()    
            if current_bp == 0x401dc0:
                url_data_offset = 0x413c7c
                lstrlen_addr = 0x401e57
                decrypt_download_url(url_data_offset, lstrlen_addr)
                adapter.breakpoint_clear(current_bp)

            # Decrypt strings in mw_save_to_disk()
            if current_bp == 0x4013a0:
                decrypt_save_to_disk_strings()
                adapter.breakpoint_clear(current_bp)
                
            # Decrypt strings in mw_start_process()
            if current_bp == 0x401607:
                decrypt_process_name()
                adapter.quit()
                break

if __name__ == "__main__":
    main()