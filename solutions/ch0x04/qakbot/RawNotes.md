I need to figure out how to force the malware sample to always take the path to decrypt the resource 

When executing from the original sample:  
* Set breakpoint on VirtualAllocEx  
* Unpacker decryptes a resource and the transfer control to the decrypted code. This is not stage2 payload yet. This is different than when I unpacked it. Actually two things are different. I renamed the original sample to `wqigacu.exe` and I actually set the original bp on VirtualProtect.  


* When unpacking payload the original PE is over written. Once unpacking is finished transfer ultimatly transfered to address 0x401a5b (_start stage1.bin)

* How do i automatically find that address to VirtualProtect so i can set a conditional breakpoint when this api is called?
* Turns out the broader question is how do API resolution without GetProcAddress, LoadLibrary, or GetModuleHandle since Binja doesn't natively support the ability to set breakpoints on DLL exports by name?  

## Checkpoint: 2022-04-22 06:12 EDT  
* I had to get familar with how qbot unpacks itself  
* Research on PE header and how exports work and how they are forwarded  
* I can use the debugger to get a list of all of the loaded modules  
* I was able to use `lief` to extract the imagesize i need to read the module in memory  
* I'm able to create a view of the loaded module but for some reason I having a ton of issues trying to parse the export table  
* The reason I need to get this to work is because I basically have to implement my own GetProcAddress for my debugger solution to work  


## Checkpoint: 2022-04-23 04:17 EDT  
* Discovered that `pefile` has a `get_memory_mapped_image()` method that may allow me to open the dll on disk and using the base address from `adapter.mem_modules()` and get the memory mapped layout of the DLL so I can possibly find the exports  


## Loaded modules during current exection

C:\Windows\System32\wow64.dll->[0x657c0000, 0x52000]  
C:\Windows\System32\wow64cpu.dll->[0x65820000, 0xa000]  
C:\Windows\System32\wow64win.dll->[0x65830000, 0x77000]  
C:\Windows\SysWOW64\CRYPTBASE.dll->[0x740f0000, 0xa000]  
C:\Windows\SysWOW64\SspiCli.dll->[0x74100000, 0x1f000]  
C:\Windows\SysWOW64\gdi32full.dll->[0x74190000, 0x15a000]  
C:\Windows\SysWOW64\win32u.dll->[0x742f0000, 0x15000]  
C:\Windows\SysWOW64\msvcrt.dll->[0x74310000, 0xbe000]  
C:\Windows\SysWOW64\shlwapi.dll->[0x74450000, 0x46000]  
C:\Windows\SysWOW64\sechost.dll->[0x744b0000, 0x41000]  
C:\Windows\SysWOW64\GDI32.dll->[0x745f0000, 0x2b000]  
C:\Windows\SysWOW64\RPCRT4.dll->[0x74690000, 0xc1000]  
C:\Windows\SysWOW64\windows.storage.dll->[0x74760000, 0x56e000]  
C:\Windows\SysWOW64\KERNEL32.DLL->[0x74cd0000, 0xe0000]  
C:\Windows\SysWOW64\combase.dll->[0x74db0000, 0x211000]  
C:\Windows\SysWOW64\profapi.dll->[0x74fd0000, 0xf000]  
C:\Windows\SysWOW64\kernel.appcore.dll->[0x74fe0000, 0xd000]  
C:\Windows\SysWOW64\powrprof.dll->[0x74ff0000, 0x45000]  
C:\Windows\SysWOW64\ADVAPI32.dll->[0x75220000, 0x77000]  
C:\Windows\SysWOW64\SHELL32.dll->[0x75470000, 0x13d8000]  
C:\Windows\SysWOW64\cfgmgr32.dll->[0x768a0000, 0x36000]  
C:\Windows\SysWOW64\shcore.dll->[0x76900000, 0x88000]  
C:\Windows\SysWOW64\USER32.dll->[0x76990000, 0x15f000]  
C:\Windows\SysWOW64\IMM32.DLL->[0x76d10000, 0x25000]  
C:\Windows\SysWOW64\KERNELBASE.dll->[0x77260000, 0x1a1000]  
C:\Windows\SysWOW64\bcryptPrimitives.dll->[0x774a0000, 0x5a000]  
C:\Windows\SysWOW64\ucrtbase.dll->[0x77500000, 0xe0000]  
C:\Windows\SysWOW64\ntdll.dll->[0x7ffd449e0000, 0x183000]  

## Checkpoint: 2022-04-23 18:22 EDT  
* Removed the need to use `get_memory_mapped_image()`  
* Got my version of `GetProcAddress` implemented but its a bit slow  
* Learned ImageBase + Export RVA = Export Address (absolute)  


## Unpacking
* Break on VirtualProtect...When BP is hit get the following    
  * Get size of size of unpacked PE file... its the 2nd arg to VirtualProtect  (ESP + 8 = 0x37000)  
  * Get Return address from top of stack
  * Execute to return and read address at the top of the stack... This is the unpacked stage1 payload.  
  
  
## Resource Extraction  
* A good chunk of the code i wrote for the unpacker can be used... When i get this working i wonder if it makes sense to just turn this into a plugin?  I would need to make this general enough to where i don't rely on direct addressing.  

* Extract the following:  
  * resource name  
  * the resource itself (after decryption and decompression)  
  * Veryify the resource is correctly extracted  
  

## Checkpoint 2022-05-22 13:54 EDT  
* I noticed that at some point a thread executing ntdll.dll starts, usually seems to happend before my breakpoints ever hit.  

## Checkpoint: 2022-05-02 14:27 EDT  
* Vector35 just updated the debugger and integrated it with the core Binja app in the dev branch. Looks like i will be testing the new debugger and refactoring the unpacker.  
* Core debugger still doesn't have direct support to add breakpoints on API names  
* Found a bug in the debugger over the weekend. Report was filed and fix has been released in dev  

## Checkpoint: 2022-05-02 18:11 EDT  
* The new debugger needs shit ton of more work. its a little to frustrating to deal with it right now.  
* I disabled the native debugger and reverted back to used the old plugin for now  

## Checkpoint 2022-05-04 15:40 EDT  
* Most of the unpacking code can be reused for the resourse extraction  
* I need to figure out how to generalize the logic enough that i can use this for any qbot sample  
* How do i automatically find the addresses of the functions used in the resourse extraction??? Maybe Yara or something idk  

## Checkpoint 2022-05-18 20:00 EDT  
* I need to take a step back. I think the reason why i have been struggling is because its dectecting my VM environment. I need to fully reverse the anti-analysis checks. Starting to wonder if my extractor will need to have anti-anti-vm-evasions put in. I been so focused on thinking it was detecting my debugger. I should have stepped away sooner.  

## Checkpoint 2022-05-21 13:32 EDT  
* I just finished reversing all of the vmcheck's. I probably should have done that to begin with. Now i see that qbot possibly takes different code execution path depending on whether its in a virtual or physical environment. Back to the debugger.  


## Checkpoint 2022-05-22 13:54 EDT  
* I noticed that at some point a thread executing ntdll.dll starts, usually seems to happend before my breakpoints ever hit.  

* Looks likes there is some junk code at addresses 0x401680 and 0x401685. Code does unnessary calculations on eax before eax is passed to mw_decrypt_strings(). I need to get the constant at the address just before the call to mw_decrypt_strings().  


* So to get this to work I need:
  * Set `EAX` to 0x358f at call to mw_decrypt_strings() (eax = eax + \<CONST>)
  * Set resource type to RC_ICON(0x3)...2nd param to FindResourceA (esp + 8)  
  
* How the fuck do you update the binaryview to reflect the new overwritten PE file??? The address that i need to jump to is technically not in the current view. Its in the bv2 view because that is the unpacked payload. I'm at a weird cross road... Do i implement this using the new beta-released debugger since it knows how to automatically update the view or do i try to find some weird hack that prob isn't very elagant to get this to work. 

## Checkpoint 2022-05-25 14:57 EDT  
* After long internal debate i think im just gonna say fuck it and force myself to use the new debugger even though its only in beta testing right now. I think given all of the issues i know im gonna have initially, its the better solution in the long run. Im gonna have to refactor my qbot unpacker.  

## Checkpoint 2022-05-28 18:54 EDT  
* I've had to take a massive detour. Since i decided to go with the new binja debugger i've been doing more bug squashing then dev'ing. There are core features that the debugger supports that I need but getting through all the hiccups in dev is a little painful (but fun to!). Current issues i'm finding seem to revolve around I/O sync issues between the debugger and the UI. So far i've found 4 bugs two have been fixed!  

## Loaded modules during current execution... Note I turned ASLR back on for the original sample. In order to rebase the binary i need this feature on  
<DebugModule: image00000000`00400000, 0x400000, 0x2af000>  
<DebugModule: C:\Windows\System32\wow64.dll, 0x657c0000, 0x52000>  
<DebugModule: C:\Windows\System32\wow64cpu.dll, 0x65820000, 0xa000>  
<DebugModule: C:\Windows\System32\wow64win.dll, 0x65830000, 0x77000>  
<DebugModule: C:\Windows\SysWOW64\uxtheme.dll, 0x73b80000, 0x75000>  
<DebugModule: C:\Windows\SysWOW64\WKSCLI.DLL, 0x73d60000, 0x10000>  
<DebugModule: C:\Windows\SysWOW64\LOGONCLI.DLL, 0x73d70000, 0x2f000>  
<DebugModule: C:\Windows\SysWOW64\SAMCLI.DLL, 0x73da0000, 0x15000>  
<DebugModule: C:\Windows\SysWOW64\NETUTILS.DLL, 0x73dc0000, 0xb000>  
<DebugModule: C:\Windows\SysWOW64\netapi32.dll, 0x73dd0000, 0x13000>  
<DebugModule: C:\Windows\SysWOW64\USERENV.dll, 0x73df0000, 0x1a000>  
<DebugModule: C:\Windows\SysWOW64\bcrypt.dll, 0x73e20000, 0x1b000>  
<DebugModule: C:\Windows\SysWOW64\CRYPTBASE.dll, 0x740f0000, 0xa000>  
<DebugModule: C:\Windows\SysWOW64\SspiCli.dll, 0x74100000, 0x1f000>  
<DebugModule: C:\Windows\SysWOW64\gdi32full.dll, 0x74190000, 0x15a000>  
<DebugModule: C:\Windows\SysWOW64\win32u.dll, 0x742f0000, 0x15000>  
<DebugModule: C:\Windows\SysWOW64\msvcrt.dll, 0x74310000, 0xbe000>  
<DebugModule: C:\Windows\SysWOW64\shlwapi.dll, 0x74450000, 0x46000>  
<DebugModule: C:\Windows\SysWOW64\sechost.dll, 0x744b0000, 0x41000>  
<DebugModule: C:\Windows\SysWOW64\ole32.dll, 0x74500000, 0xec000>  
<DebugModule: C:\Windows\SysWOW64\GDI32.dll, 0x745f0000, 0x2b000>  
<DebugModule: C:\Windows\SysWOW64\RPCRT4.dll, 0x74690000, 0xc1000>  
<DebugModule: C:\Windows\SysWOW64\windows.storage.dll, 0x74760000, 0x56e000>  
<DebugModule: C:\Windows\SysWOW64\KERNEL32.DLL, 0x74cd0000, 0xe0000>  
<DebugModule: C:\Windows\SysWOW64\combase.dll, 0x74db0000, 0x211000>  
<DebugModule: C:\Windows\SysWOW64\profapi.dll, 0x74fd0000, 0xf000>  
<DebugModule: C:\Windows\SysWOW64\kernel.appcore.dll, 0x74fe0000, 0xd000>  
<DebugModule: C:\Windows\SysWOW64\powrprof.dll, 0x74ff0000, 0x45000>  
<DebugModule: C:\Windows\SysWOW64\ADVAPI32.dll, 0x75220000, 0x77000>  
<DebugModule: C:\Windows\SysWOW64\SHELL32.dll, 0x75470000, 0x13d8000>  
<DebugModule: C:\Windows\SysWOW64\cfgmgr32.dll, 0x768a0000, 0x36000>  
<DebugModule: C:\Windows\SysWOW64\shcore.dll, 0x76900000, 0x88000>  
<DebugModule: C:\Windows\SysWOW64\USER32.dll, 0x76990000, 0x15f000>  
<DebugModule: C:\Windows\SysWOW64\IMM32.DLL, 0x76d10000, 0x25000>  
<DebugModule: C:\Windows\SysWOW64\setupapi.dll, 0x76e50000, 0x40b000>  
<DebugModule: C:\Windows\SysWOW64\KERNELBASE.dll, 0x77260000, 0x1a1000>  
<DebugModule: C:\Windows\SysWOW64\bcryptPrimitives.dll, 0x774a0000, 0x5a000>  
<DebugModule: C:\Windows\SysWOW64\ucrtbase.dll, 0x77500000, 0xe0000>  
<DebugModule: ntdll.dll, 0x77640000, 0x183000>  
<DebugModule: ntdll.dll, 0x7ffd449e0000, 0x1d2000>  

## Checkpoint 2022-06-04 04:42 EDT  
* After a few PM exchanges with the awesome Binaryninja team looks like in order to fix the I/O issues i've been having they are going to implement pipe comms between binja and the dbgeng. They will be pushing a bug fix to dev so just after Vector35 gets past the stable 3.1 release this week. 

## Checkpoint 2022-06-04 16:29 EDT  
* Version 3.1.3490-dev (2022-06-08 07:37:55)- Includes the latest debugger release that fixes some of the i/o sync issues. During testing noticed that the snippet editor is still closes when the debugger launches. A couple times it did not so not sure what the cause is. 

## Checkpoint 2022-06-11 08:14 EDT  
* Still issues with the snippet editor being destroyed however i did port the qbot unpacker to use the new debugger. Now the issue is file writes and printing to stdin are effected since moving to pipe comms. I have tested this both in the windows cli and snippet editor.  At this point i explained the problem in the BN debugger slack channel.

## Checkpoint 2022-06-12 11:49 EDT  
* I needed to use the `dbg.go_and_wait()` api now that proc / debugger comms have been changed in BN. I am now able to successfully unpack qbot samples with this. I don't rely on a single address in this new solution!!!
* Now i can finally focus on writing the qbot resource payload and config extractor  
* Reminder * So to get this to work I need:
  * Set `EAX` to 0x358f at call to mw_decrypt_strings() (eax = eax + \<CONST>)
  * Set resource type to RC_ICON(0x3)...3rd param to FindResourceA (esp + 8)
  
## Checkpoint 2022-06-22 19:46 EDT  
* Submitted a BN support request... Finding that the BN analysis engine goes into some weird loop trying to update the binaryview in cases when the original PE header is overwritten. I'll have to follow this detour until its fixed  


## Checkpoint 2022-09-24 17:28 EDT  
* Its been a few months and I almost forgot where i left off. Currently I am working on bypassing the anti-analysis checks. I need to solve this problem first before I can get sample to call the payload extract function.  

## Checkpoint 2022-09-25 11:53 EDT  
* At 0x401685 i nop'ed out `imul    eax, dword [data_410f34]`. I use this to bypass the junk result saved in EAX prior to `add     eax, 0x358f`  
* At 0x4040cf `push    eax {var_20_1}` I need to set the value to 0x3 for the RC_ICON type. This is the 3rd param to FindResourceA  

