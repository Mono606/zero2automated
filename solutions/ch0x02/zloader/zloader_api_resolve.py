#!/usr/bin/env python3
import sys
import json
from binaryninja import *


# Export dll script used to build the export database dll names to resolve: https://gist.github.com/OALabs/94ff4fc02bf02d55a8161068cafd11c0


export_db = {}

def setup(json_file):
    global export_db
    
    exports_json = json.loads(open(json_file,'r').read())
    exports_list = exports_json['exports']
    
    for export in exports_list:
        api_hash = calc_api_hash(export)
        export_db[api_hash] = export

def calc_api_hash(import_name):
    mask = 0xf0000000
    ret = 0
    
    for c in import_name:
        ret = ord(c) + (ret << 0x4)
        if (ret & mask):
            ret = (~ret | mask) ^ (~ret | ~mask) >> 0x18
    
    return ret & 0xfffffff

def main():
    export_json_path = "exports.json"
    setup(export_json_path)

    bndb_path = "zloader_unpacked-BN.bndb"
    bv = BinaryViewType.get_view_of_file(bndb_path)
    bv.update_analysis()


    # Write back to same file
    bv.create_database(bndb_path)

    mw_api_name_resolve_addr = 0x2fc5570
    xrefs = bv.get_code_refs(mw_api_name_resolve_addr)

    for xref in xrefs:
        il = xref.function.get_low_level_il_at(xref.address).mlil
        if il.operation == MediumLevelILOperation.MLIL_CALL:
            api_const = il.params[1].value.value
            resolved_import = export_db.get(api_const)
            if (api_const and resolved_import) != None:
                dll = export_db.get(api_const)
                bv.set_comment_at(xref.address, dll)
                print("{0}->{1}".format(hex(api_const), dll))

    bv.save_auto_snapshot()

if __name__ == "__main__":
    main()

